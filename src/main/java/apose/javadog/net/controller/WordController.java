package apose.javadog.net.controller;

import apose.javadog.net.entity.BaseInfo;
import apose.javadog.net.entity.Education;
import apose.javadog.net.entity.Interview;
import apose.javadog.net.entity.WorkExperience;
import cn.hutool.core.util.CharsetUtil;
import com.aspose.words.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: yang_li
 * @Date: 2023/11/1710:27
 */
@RestController
@RequestMapping("/word")
@Slf4j
public class WordController {

    @GetMapping("/pdf")
    void pdf(HttpServletResponse response){
        // 获取资源doc路径下的简历interview.doc模板
        ClassPathResource classPathResource = new ClassPathResource("doc\\interview.doc");
        // 组装数据
        Document doc;
        File file = new File("D:\\decore\\web\\server\\test.pdf");

             // 将流输入到自己创建的文件中
        try (OutputStream outputStream1 = new FileOutputStream(file);
             // 将流输入到response 然后生成文件
             OutputStream outputStream = response.getOutputStream();
             InputStream inputStream = classPathResource.getInputStream()){
            // 文件名称
            // String fileName = URLEncoder.encode("帅锅的简历.pdf", CharsetUtil.UTF_8);
            // response.reset();
            // response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            // response.setHeader("Access-Control-Allow-Origin", "*");
            // response.setContentType("application/octet-stream;charset=UTF-8");

            // 将输入流转为doc
            doc = new Document(inputStream);

            // 让每一个格子都自适应大小  这里只能支持word    如果需要pdf中的格子也自适应 需要先生成word  然后将word转成pdf
            int pageCount = doc.getPageCount();
            for (int i = 0; i < pageCount; i++) {
                Table table = (Table) doc.getChild(NodeType.TABLE, i, true);
                if (table != null) {
                    table.autoFit(AutoFitBehavior.FIXED_COLUMN_WIDTHS);
                }
            }
            // doc构建
            DocumentBuilder builder = new DocumentBuilder(doc);

            // 定位书签位置
            builder.moveToBookmark("AVATAR");
            // 插入图片
            builder.insertImage("D:\\ELSE\\pictures\\else\\myFriends\\IMG_2852.png");

            // 定位书签位置
            builder.moveToBookmark("LANGUAGE_LEVEL3");
            // 插入图片
            Shape shape = builder.insertImage("D:\\ELSE\\pictures\\else\\myFriends\\IMG_2852.png");
            shape.setHeight(64);
            shape.setWidth(43);

            // 定位LANGUAGE_LEVEL4书签位置
            builder.moveToBookmark("LANGUAGE_LEVEL4");
            // 设置字符名称
            builder.getFont().setName("Wingdings");
            // 设置字符大小
            builder.getFont().setSize(14);
            // 对号字符
            builder.write("\uF0FE");

            // 定位LANGUAGE_LEVEL6书签位置
            builder.moveToBookmark("LANGUAGE_LEVEL6");
            // 设置字符名称
            builder.getFont().setName("Wingdings");
            builder.getFont().setSize(20);
            builder.write("□");

            // 提交设置的属性
            doc.updateFields();
            final ReportingEngine engine = new ReportingEngine();

            // 将数据填充至模板
            engine.buildReport(doc, getInterviewData(), "data");

            // 导出word  名字后缀需要改成.doc   或者  .docx
            // 使用 doc.save(outputStream, SaveFormat.WORD_ML); 转word

            // 转pdf 或者docx ...
            doc.save(outputStream1, SaveFormat.PDF);
            log.info("====== pdf 打印成功 ======");
        } catch (Exception e) {
            log.error("生成报告异常，异常信息：{}", e.getMessage(), e);
            e.printStackTrace();
        }
    }

    private Interview getInterviewData(){
        Interview interview = new Interview();
        this.getBaseInfo(interview);
        this.getEducations(interview);
        this.getWorkExperiences(interview);
        return interview;
    }

    /**
     * 组装基本数据
     * @param interview 。
     */
    private void getBaseInfo(Interview interview){
        // 基本数据
        BaseInfo baseInfo = new BaseInfo();
        java.util.List<String> listStr = new ArrayList<>();
        listStr.add("后端技术栈：有较好的Java基础，熟悉SpringBoot,SpringCloud,springCloud Alibaba等主流技术，Redis内存数据库、RocketMq、dubbo等，熟悉JUC多线程");
        listStr.add("后端模板引擎：thymeleaf、volocity");
        listStr.add("前端技术栈：熟练掌握ES5/ES6/、NodeJs、Vue、React、Webpack、gulp");
        listStr.add("其他技术栈: 熟悉python+selenium、electron");
        baseInfo.setName("狗哥")
                .setBirth("1993年5月14日")
                .setHeight("180")
                .setWeight("70")
                .setNation("汉")
                .setSex("男")
                .setNativePlace("济南")
                .setMarriage("已婚")
                .setSpecialtys(listStr);
        interview.setBaseInfo(baseInfo);
    }
    /**
     * @Description: 组装教育经历
     * @Param: [interview]
     */
    private void getEducations(Interview interview){
        // 高中
        java.util.List<Education> educations = new ArrayList<>();
        Education education = new Education();
        education.setStartEndTime("2009-2012")
                .setSchool("山东省实验中学")
                .setFullTime("是")
                .setProfessional("理科")
                .setEducationalForm("普高");
        educations.add(education);
        // 大学
        Education educationUniversity = new Education();
        educationUniversity.setStartEndTime("2012-2016")
                .setSchool("青岛农业大学")
                .setFullTime("是")
                .setProfessional("计算机科学与技术")
                .setEducationalForm("本科");
        educations.add(educationUniversity);
        interview.setEducations(educations);
    }

    /**
     * @Description: 组装工作经历
     * @Param: [interview]
     */
    private void getWorkExperiences(Interview interview){
        // 工作记录
        List<WorkExperience> workExperiences = new ArrayList<>();
        WorkExperience workExperience = new WorkExperience();
        workExperience.setStartEndTime("2009-2012")
                .setWorkUnit("青岛XXX")
                .setPosition("开发")
                .setResignation("有更好的学习空间，向医疗领域拓展学习纬度");
        workExperiences.add(workExperience);
        interview.setWorkExperiences(workExperiences);
    }
}
