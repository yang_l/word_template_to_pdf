package apose.javadog.net.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author: yang_li
 * @Date: 2023/11/1710:27
 */
@Data
@Accessors(chain = true)
public class WorkExperience {
    /**
     * 起止时间
     */
    private String startEndTime;

    /**
     * 工作单位
     */
    private String workUnit;

    /**
     * 职位
     */
    private String position;

    /**
     * 离职原因
     */
    private String resignation;

}
