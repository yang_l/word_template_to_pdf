package apose.javadog.net.entity;
import lombok.Data;

import java.util.List;

/**
 * @Author: yang_li
 * @Date: 2023/11/1710:27
 */
@Data
public class Interview {

    private BaseInfo baseInfo;

    private List<Education> educations;

    private List<WorkExperience> workExperiences;

}
