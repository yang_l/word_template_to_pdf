package apose.javadog.net;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: yang_li
 * @Date: 2023/11/1710:27
 */
@SpringBootApplication
public class BootAposeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootAposeApplication.class, args);
    }

}
